# FHS-16S

This repository contains the code to reproduce the analysis in the study investigating the  gut microbiota in the Framingham Heart Study cohort, including statistical analyses to investigate trends in overall microbiome composition and diversity in relation to disease states and systematically examined taxonomic associations with a variety of clinical traits, disease phenotypes, clinical blood markers, and medications

The results are presented in the paper
Population study of the gut microbiome: associations with diet, lifestyle and cardiometabolic disease
by
Rebecca L. Walker, Hera Vlamakis, Jonathan Wei Jie Lee, Luke A. Besse, Vanessa Xanthakis, Ramachandran S. Vasan, Stanley Y. Shaw, Ramnik J. Xavier
(2021, submitted)
